# Jira Exporter

Exports User Stories into beautiful LaTeX-Code.


![user story](img/tex.png)


*Sample User Story*

## Code Requirements

To run the `Jira_Exporter` program, you will need a JSON File called `data.json` with your credentials. 

It needs following structure:

```json
{
  "server": "your_serverurl",
  "user": "your_username",
  "password": "your_user_password",
  "query": "your_query"
}
```

The query defines, which Jira-issues are going to be used and converted to LaTeX-code. A query can have following syntax:

```
project=[project_name] and sprint=\"[sprint_name]\"
```

*note:* Don´t  use \[ or \] in the real query, it´s just a syntax helper.

You can find more about queries in the official JQL documentation. 

## Latex Requirements

Follow command  is needed in a `tex` or `cls` File:

```latex
\newcommand{\userStory}[8]{
    \renewcommand{\arraystretch}{1.2}
    \begin{tcolorbox}[tabularx={p{4cm} X}, adjusted title=\smash{\raisebox{-15.1pt}{\includegraphics[width=0.5cm,height=0.5cm]{img/us.PNG}}} \hspace{0.68cm} {\tikz \node(char)[transform canvas={yshift=1mm}][draw=orange,fill=white,
         shape=rectangle, very thick, align=center, text centered, text depth=0.4ex,text height=1.5mm, text=orange]{#1};} \hspace{3mm} {\LARGE  #2},skin=freelance,
    colback=white,colframe=black,
    frame code={\path[draw=orange,fill=orange]
    (frame.south west) rectangle (frame.north east);}]
        \textbf{Zugewiesen} & \member{#3} \\
        \textbf{Akzeptanzkriterium}	 & \\
        \hspace{0.2cm}\textit{Als}& #4\\
        \hspace{0.2cm}\textit{möchte ich}& #5\\
        \hspace{0.2cm}\textit{damit}& #6\\
        \textbf{Schätzung} & \\
        \multicolumn{2}{p{\textwidth-2\tabcolsep}}{#7}\\
        \textbf{Status} & \\
        \multicolumn{2}{p{\textwidth-2\tabcolsep}}{#8}\\
    \end{tcolorbox} 
    \vspace{0.5cm}
    \renewcommand{\arraystretch}{1}
}
```

## Jira Requirements

The Jira Exporter needs a special syntax in your User Stories for proper functioning. The description needs following style:


![user story](img/user_story.PNG)


The first `#` will be built as the so called *Akzeptanzkriterien* in the document. 

1. `#` - *Als* ...
2. `#` - *möchte ich* ...
3. `#` - *damit* ...

The first `//` marks the paragraph for the section *Schätzung*

The second `//` marks the paragraph for the section *Status*

## Running the exporter

When you meet the requirements, just run `Jira_Exporter.py` via Python. While running the program will prompt you to choose a directory, where the output will be saved. 

When the program finishes, you will see a new file `jira_exporter.txt` in your marked directory.

Just copy and paste the content of your file into your LaTeX document. That´s it.

## Sources

* [JQL](https://www.atlassian.com/blog/jira-software/jql-the-most-flexible-way-to-search-jira-14)
* [Jira-Api](https://jira.readthedocs.io/en/master/examples.html#issues)