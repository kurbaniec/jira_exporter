from jira import JIRA
from tkinter import filedialog
from tkinter import *
import json


class tex_issues():
    def __init__(self):
        self.issues = list()

    def add_issue(self, issue):
        self.issues.append(issue)

    def get_issues(self):
        self.issues.sort()
        return self.issues

class tex_issue():
    def __init__(self, points="NA", key="NA--0", name="NA", reporter="NA", criteria=["NA", "NA", "NA"], guess="NA", status="NA"):
        try:
            self.points = int(points) if points is not None else 0
        except:
            self.points = 0
        self.key = key if key is not None else "WEB NA"
        self.name = name if name is not None else "NA"
        self.reporter = reporter if reporter is not None else "NA"
        tmp = ["NA", "NA", "NA"]
        if isinstance(criteria, list):
            if criteria[0] is not None or criteria[0] != "":
                tmp[0] = criteria[0].lstrip()
            if criteria[1] is not None or criteria[1] != "":
                tmp[1] = criteria[1].lstrip()
            if criteria[2] is not None or criteria[2] != "":
                tmp[2] = criteria[2].lstrip()
        self.criteria = tmp
        self.guess = guess if guess is not None else "NA"
        self.status = status if status is not None else "NA"
        self.status = self.status.replace("\r\n\r\n", "")

    def latex(self):
        return "\\userStory{" + str(self.points) + "}" \
                "{" + self.key + " | " + self.name + "}" \
                "{" + self.reporter + "}\n" \
                "{" + self.criteria[0] + "}\n{" + self.criteria[1] + "}\n{" + self.criteria[2] + "}\n" \
                "{" + self.guess + "}\n" \
                "{" + self.status + "}"

    def __lt__(self, other):
        return int(self.key[4:]) < int(other.key[4:])

    @staticmethod
    def description_factory(description):
        if description != None:
            description = description.replace("\n", "").replace("\r", "")
            output = list()
            description.lstrip().rstrip()
            d = description.split("//")
            for i in range(len(d)):
                d[i].lstrip().rstrip()
            guess = d[0].lstrip("#").rstrip().split("#")
            #for i in range(len(guess)):
            #    guess[i] = guess[i]
            output.append(guess)
            output.append(d[1].lstrip())
            output.append(d[2].lstrip())
            return output
        else:
            return [["A1", "A2", "A3"], "B", "C"]

# Ask where output should be saved
def browse_button():
    filename = filedialog.askdirectory()
    print(filename)
    return filename

# Import user data `data.json`
def import_data():
    with open("data.json") as data:
        return json.load(data)


if __name__ == "__main__":
    # Get all data
    data = import_data()

    # Connect
    jira = JIRA(server=data["server"], auth=(data["user"], data["password"]))

    # Get all issues from query result
    issues = jira.search_issues(data["query"])

    # Convert issue to needed form
    iIssues = tex_issues()
    for issue in issues:
        iPoints = issue.fields.customfield_10023        #print(iPoints)
        iKey = issue.key                                #print(iKey)
        iName = issue.fields.summary                    #print(iName)
        iReporter = "NA"
        try:
            iReporter = issue.fields.assignee.displayName   #print(iReporter)
        except Exception as e:
            print(e)
        iDescription = issue.fields.description         #print(iDescription)
        iPacked = tex_issue.description_factory(iDescription)
        iIssues.add_issue(tex_issue(iPoints, iKey, iName, iReporter, iPacked[0], iPacked[1], iPacked[2]))


    jira.close()

    root = Tk()
    root.withdraw()

    file_path = filedialog.askdirectory()
    print(file_path)

    print("\n########\n")

    text_output = ""
    for i in iIssues.get_issues():
        out = i.latex()
        print(i.latex() + "\n")
        text_output += str(i.latex()) + "\n"

    with open(file_path+"/jira_exporter.txt", "w") as file:
        file.write(text_output)
